//
//  ViewController.m
//  CustomTapView
//
//  Created by iOS on 2017/3/21.
//  Copyright © 2017年 iOS. All rights reserved.
//

#import "ViewController.h"
#import "LGRoundButton.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    LGRoundButton *btn = [[LGRoundButton alloc] initWithFrame:CGRectMake(50, 50, 200, 200)];
    [btn setImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    [btn setTitle:@"test" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
}

- (IBAction)tapAction:(id)sender {
    NSLog(@"点击了Btn");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

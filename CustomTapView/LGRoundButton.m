//
//  LGRoundButton.m
//  CustomTapView
//
//  Created by iOS on 2017/3/21.
//  Copyright © 2017年 iOS. All rights reserved.
//

#import "LGRoundButton.h"

@implementation LGRoundButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
 
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    NSLog(@"%@", NSStringFromCGRect(rect));
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(ctx, [UIColor orangeColor].CGColor);
//    CGContextFillEllipseInRect(ctx, rect);//椭圆
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width/2,height/2) radius:MIN(width/2, height/2) startAngle:0 endAngle:2*M_PI clockwise:NO];
    
    CGContextAddPath(ctx, path.CGPath);
    CGContextAddRect(ctx, CGRectMake(20, 20, self.bounds.size.width-40, self.bounds.size.height-40));
    CGContextSetRGBFillColor(ctx, 255, 0, 0, 1);
    
    
}


//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    
//}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    unsigned char pixel[4] = {0};
    [self RGBA:pixel ofPoint:point];
    
    for (int i=0;i<4; i++) {
        NSLog(@"%d == %d",i,pixel[i]);
    }
    NSLog(@"\n");
    if (pixel[3] == 0) {
        return NO;
    }
    
    return [super pointInside:point withEvent:event];
}

- (void)RGBA:(unsigned char[4])pixel ofPoint:(CGPoint)point
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -point.x, -point.y);
    
    [self.layer renderInContext:context];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
}

@end
